﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace parkingObjects
{
    [Serializable]
    [DataContract]
    public class Transaction
    {
        public Transaction(string regNum, decimal amount, DateTime time)
        {
            Identifier = Guid.NewGuid();
            TransportRegNum = regNum;
            Amount = amount;
            Time = time;
        }
        [DataMember]
        public Guid Identifier { get; set; }
        [DataMember]
        public DateTime Time { get; set; }
        [DataMember]
        public string TransportRegNum { get; set; }
        [DataMember]
        public decimal Amount { get; set; }
        public bool BackedUp { get; set; }
    }
}
