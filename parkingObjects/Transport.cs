﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace parkingObjects
{
    [Serializable]
    [DataContract]
    public class Transport
    {
        public Transport(string r, string t, decimal b)
        {
            RegNum = r;
            Type = t;
            TransportAccount = new Account(b);
        }
        [DataMember]
        public string RegNum { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        private Account TransportAccount { get; set; }
        [DataMember]
        public decimal ActualBalance { get { return TransportAccount.Balance; } }

        public void Deposit(decimal amount)
        {
            TransportAccount.Deposit(amount);
        }

        public void Withdraw(decimal amount)
        {
            TransportAccount.Withdraw(amount);
        }
    }
}
