﻿using parkingObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace parkingClient
{
    class Program
    {
        static HttpClient client = new HttpClient();

        static string legend = @"Welcome to Parking!
Please input number for corresponding action: 
1 - Current parking Balance
2 - Revenue for the last minute
3 - Free/Occupied number of slots
4 - All parking transactions for the last minute
5 - All parking transactions
6 - List all parked transport
7 - Park transport
8 - Take away transport
9 - Deposit transport balance
10 - Exit
and press Enter";
        static bool keepWorking = true;

        static void Main(string[] args)
        {
            // Update port # in the following line.
            client.BaseAddress = new Uri("http://localhost:40458/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            Console.WriteLine(legend);
            Console.WriteLine();
            string userInput;
            while (keepWorking)
            {
                try
                {
                    Console.WriteLine();
                    Console.WriteLine("Please input number for next action or L for legend");
                    userInput = Console.ReadLine();
                    RunAsync(userInput).GetAwaiter().GetResult();
                    Console.WriteLine();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                }
            }
            Console.WriteLine("Parking is closing, press any key");
            Console.ReadLine();
        }

        static async Task RunAsync(string userInput)
        {
            try
            {
                switch (userInput)
                {
                    case "1":
                    case "2":
                    case "3":
                        Console.WriteLine(await GetStringAsync($"/api/parking/{userInput}"));
                        break;
                    case "4":
                        foreach (var t in await GetListAsync<Transaction>("/api/transaction"))
                            Console.WriteLine($"id: {t.Identifier}, RegNum: {t.TransportRegNum}, Amount: {t.Amount.ToString("0.00")}, Time: {t.Time}");
                        break;
                    case "5":
                        Console.WriteLine("Here is the list of transactions from file:");
                        foreach (var t in await GetListAsync<string>("/api/transaction/file"))
                            Console.WriteLine(t);
                        break;
                    case "6":
                        Console.WriteLine("Here is the list of parked transport:");
                        foreach (var t in await GetListAsync<Transport>("/api/transport")) 
                            Console.WriteLine($"RegNum: {t.RegNum}, Type: {t.Type}");
                        break;
                    case "7":
                        Console.WriteLine("Please input your Registration number");
                        string rAdd = Console.ReadLine();

                        Console.WriteLine($"Please select your transport type: { await GetStringAsync($"/api/transport/all")}");
                        string type = Console.ReadLine();

                        Console.WriteLine("Please enter amount you want to deposit, if no, put 0");
                        decimal balInit;
                        while (!decimal.TryParse(Console.ReadLine(), out balInit))
                        {
                            Console.WriteLine("Please enter valid amount");
                        }
                        Console.WriteLine(await CreateTransportAsync(new Transport(rAdd, type, balInit)));                        
                        break;
                    case "8":
                        Console.WriteLine("Please input your Registration number");
                        Console.WriteLine(await DeleteTransportAsync(Console.ReadLine()));
                        break;
                    case "9":
                        Console.WriteLine("Please enter $ amount you want to deposit");
                        decimal bal;
                        while (!decimal.TryParse(Console.ReadLine(), out bal))
                        {
                            Console.WriteLine("Please enter valid amount");
                        }
                        Console.WriteLine("Please input your Registration number");
                        var r2 = Console.ReadLine();
                        Console.WriteLine(await UpdateTransportAsync(r2, bal));
                        break;
                    case "10":
                        keepWorking = false;
                        break;
                    case "L":
                        Console.WriteLine(legend);
                        break;
                    default:
                        Console.WriteLine("Unknown command, please press L, for legend");
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        static async Task<string> GetStringAsync(string path)
        {
            string result = String.Empty;
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsAsync<string>();
            }
            return result;
        }

        static async Task<string> CreateTransportAsync(Transport t)
        {
            HttpResponseMessage response = await client.PostAsJsonAsync(
                "api/transport/", t);
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<string>();
        }

        static async Task<string> UpdateTransportAsync(string num, decimal bal)
        {
            HttpResponseMessage response = await client.PutAsJsonAsync(
                $"api/transport/{num}", bal);
            response.EnsureSuccessStatusCode();
            
            return await response.Content.ReadAsAsync<string>();
        }

        static async Task<string> DeleteTransportAsync(string num)
        {
            HttpResponseMessage response = await client.DeleteAsync(
                $"api/transport/{num}");
            response.EnsureSuccessStatusCode();
            
            return await response.Content.ReadAsAsync<string>();
        }
        
        static async Task<List<T>> GetListAsync<T>(string path)
        {
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
                return await response.Content.ReadAsAsync<List<T>>();
            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new Exception("Sorry, no records yet");
            throw new Exception(response.ReasonPhrase);
        }

    }
}
