﻿using Microsoft.AspNetCore.Mvc;
using parkingObjects;
using parkingWEbAPI.Interfaces;
using parkingWEbAPI.Objects;
using parkingWEbAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace parkingWEbAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : Controller
    {
        private IParkingService parkingService;

        public TransactionController(IParkingService ps)
        {
            parkingService = ps;
        }
        // GET: api/values
        [HttpGet]
        public ActionResult<IEnumerable<Transaction>> Get()
        {
            List<Transaction> list = parkingService.GetTransactions();
            if (list.Count == 0)
            {
                return NotFound();
            }
            return Ok(list);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<IEnumerable<string>> Get(string id)
        {
            List<string> list = parkingService.ReadLogFile(Settings.TransactionLog);
            if (list.Count == 0)
            {
                return NotFound();
            }
            return Ok(list);
        }
    }
}
