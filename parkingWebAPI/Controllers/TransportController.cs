﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using parkingObjects;
using parkingWEbAPI.Services;
using System.Net.Http;
using System.Net;
using parkingWEbAPI.Interfaces;
using parkingWEbAPI.Objects;

namespace parkingWEbAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransportController : ControllerBase
    {
        private IParkingService parkingService;

        public TransportController(IParkingService ps)
        {
            parkingService = ps;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Transport>> Get()
        {
            List<Transport> list = parkingService.GetParkedTransport();
            if (list.Count == 0)
            {
                return NotFound();
            }
            return Ok(list);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(string id)
        {
            if (id == "all")
            {
                return string.Join(", ", Settings.Tariffs.AllKeys);
            }
            return parkingService.CheckTariff(id).ToString("0.00");
        }

        // POST api/values
        [HttpPost]
        public string Post([FromBody] Transport value)
        {
            try
            {
                parkingService.AddTransport(value);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return $"Your transport has been parked, your tariff is {parkingService.CheckTariff(value.Type)}$ per {Settings.PaymentPeriod} seconds";
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public string Put(string id, [FromBody] decimal value)
        {
            try
            {
                parkingService.DepositTransport(id, value);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "Your balance is successfully deposited!";
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public string Delete(string id)
        {
            try
            {
                parkingService.RemoveTransport(id);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "You can take away your transport now";
        }
    }
}
