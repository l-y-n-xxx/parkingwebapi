﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using parkingWEbAPI.Services;
using parkingWEbAPI.Interfaces;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace parkingWEbAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : Controller
    {
        private IParkingService parkingService;

        public ParkingController(IParkingService ps)
        {
            parkingService = ps;
        }
        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            string result = "unknown action";
            switch(id)
            {
                case 1:
                    result = $"Current parking balance = {parkingService.GetActualBalance().ToString("0.00")}$";
                    break;
                case 2:
                    result = $"Revenue for the last minute = {parkingService.GetLastRevenue().ToString("0.00")}$";
                    break;
                case 3:
                    result = $"Total capacity = {parkingService.GetCapacity()}: free = {parkingService.GetCapacity() - parkingService.GetOccupied()}, occupied = {parkingService.GetOccupied()}";
                    break;
                default:
                    break;
            }
            return result;
        }
        /*
        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
        */
    }
}
