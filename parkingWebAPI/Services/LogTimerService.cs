﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using parkingWEbAPI.Objects;
using System.IO;
using parkingObjects;
using parkingWEbAPI.Interfaces;

namespace parkingWEbAPI.Services
{
    public class LogTimerService : IHostedService, IDisposable
    {
        private Timer timer;
        private IServiceProvider scopeFactory;
        private IParkingService parkingService;

        public LogTimerService(IServiceProvider sp)
        {
            scopeFactory = sp;
            using (var scope = scopeFactory.CreateScope())
            {
                parkingService = scope.ServiceProvider.GetRequiredService<IParkingService>();
            }
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            timer = new Timer(TickTimerWriteLog, null, 60000, 60000);
            return Task.CompletedTask;
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        private void TickTimerWriteLog(object state)
        {
            using (StreamWriter w = File.AppendText(Settings.TransactionLog))
            {
                List<Transaction> list = parkingService.GetTransactionsToBackup();
                foreach (var transaction in list)
                {
                    w.WriteLine($"id:{transaction.Identifier}, regnum: {transaction.TransportRegNum}, amount:{transaction.Amount.ToString("0.00")}, time:{transaction.Time}");
                    transaction.BackedUp = true;
                }
                parkingService.CleanTransactions();
            }
        }

        public void Dispose()
        {
            timer?.Dispose();
        }
    }
}
