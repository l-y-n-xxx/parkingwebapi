﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using parkingWEbAPI.Objects;
using parkingWEbAPI.Interfaces;

namespace parkingWEbAPI.Services
{
    public class PayTimerService : IHostedService, IDisposable
    {
        private Timer timer;
        private IServiceProvider scopeFactory;
        private IParkingService parkingService;

        public PayTimerService(IServiceProvider sp)
        {
            scopeFactory = sp;
            using (var scope = scopeFactory.CreateScope())
            {
                parkingService = scope.ServiceProvider.GetRequiredService<IParkingService>();
            }
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            timer = new Timer(TickTimerPay, null, 1000, 1000);
            return Task.CompletedTask;
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        private void TickTimerPay(object state)
        {
            TimeSpan span = new TimeSpan();
            DateTime time = new DateTime();
            if (parkingService.GetParkedTransport().Count > 0)
                foreach (var parked in parkingService.GetParkedTransportAndTimes())
                {
                    time = DateTime.Now;
                    span = time - parked.Item2;
                    if (span.Seconds % Settings.PaymentPeriod == 0)
                        parkingService.ChargeTransport(parked.Item1, time);
                }
        }

        public void Dispose()
        {
            timer?.Dispose();
        }
    }
}
