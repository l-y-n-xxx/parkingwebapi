﻿using parkingObjects;
using parkingWEbAPI.Interfaces;
using parkingWEbAPI.Objects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace parkingWEbAPI.Services
{
    public class ParkingService : IParkingService
    {
        private static Parking parking;
        public ParkingService()
        {
            parking = Parking.Instance;
            ReadSettings();
        }

        public decimal GetActualBalance()
        {
            return parking.ActualBalance;
        }

        public decimal GetLastRevenue()
        {
            decimal result = 0 ;
            if (parking.TransactionsToShow.Count > 0)
                result = parking.TransactionsToShow.Select(s => s.Amount).Sum();
            return result;
        }

        public decimal GetCapacity()
        {
            return parking.Capacity;
        }

        public List<Transport> GetParkedTransport()
        {
            return parking.ParkedTransport.Select(t =>t.Item1).ToList();
        }

        public List<Tuple<Transport, DateTime>> GetParkedTransportAndTimes()
        {
            return parking.ParkedTransport;
        }

        public decimal GetOccupied()
        {
            return parking.ParkedTransport.Count;
        }

        public decimal CheckTariff(string type)
        {
            return parking.CheckTariff(type);
        }

        public void AddTransport(Transport t)
        {
            parking.AddTransport(t.RegNum, t.Type, t.ActualBalance);
        }

        public void DepositTransport(string num, decimal bal)
        {
            parking.DepositTransportByRegNum(num, bal);
        }

        public void ChargeTransport(Transport tr, DateTime time)
        {
            parking.MakeTransportPay(tr, time);
        }

        public void RemoveTransport(string num)
        {
            parking.RemoveTransportByRegNum(num);
        }

        public List<Transaction> GetTransactions()
        {
            return parking.TransactionsToShow;
        }

        public List<Transaction> GetTransactionsToBackup()
        {
            return parking.Transactions.Where(t => !t.BackedUp).ToList();
        }

        public void CleanTransactions()
        {
            parking.Transactions.RemoveAll(t => t.BackedUp && !parking.TransactionsToShow.Contains(t));
        }

        public List<string> ReadLogFile(string filename)
        {
            List<string> list = new List<string>();
            if (File.Exists(filename))
                using (StreamReader file = new StreamReader(filename))
                {
                    int counter = 0;
                    string ln;

                    while ((ln = file.ReadLine()) != null)
                    {
                        list.Add(ln);
                        counter++;
                    }
                    file.Close();
                    //Console.WriteLine($"File has {counter} lines.");
                }
            else
                list.Add("Sorry, no records yet, wait for a minute");
            return list;
        }


        static void ReadSettings()
        {
            parking.Capacity = Settings.Capacity;

            foreach (var key in Settings.Tariffs.AllKeys)
                    parking.Tariffs.Add(key, decimal.Parse(Settings.Tariffs[key], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture.NumberFormat));
            
            parking.PaymentPeriod = Settings.PaymentPeriod;
            parking.Penalty = Settings.Penalty;

            parking.Deposit(Settings.InitialBalance);

            File.Delete(Settings.TransactionLog);
        }
    }
}
