﻿using parkingObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace parkingWEbAPI.Objects
{
    public class Parking
    {
        private static readonly Parking p = new Parking();
        static Parking()
        {
        }
        private Parking()
        {
            ParkingAccount = new Account();
            Transactions = new List<Transaction>();
        }
        public static Parking Instance
        {
            get { return p; }
        }


        private List<Tuple<Transport, DateTime>> parkedTransport = new List<Tuple<Transport, DateTime>>();
        public List<Tuple<Transport, DateTime>> ParkedTransport
        {
            get { return parkedTransport; }
            set { parkedTransport = value; }
        }

        private Transport FindTransportByRegNum(string regNum)
        {
            return ParkedTransport.Where(t => t.Item1.RegNum == regNum).Select(t => t.Item1).SingleOrDefault();
        }

        public decimal CheckTariff(string type)
        {
            decimal result;
            if (!Tariffs.TryGetValue(type, out result))
                throw new ApplicationException($"Sorry, {type} can't be parked, only - {string.Join(", ", Tariffs.Keys)}!");
            return result;
        }

        public void AddTransport(string regNum, string type, decimal balance)
        {
            if (ParkedTransport.Count < Capacity)
            {
                var tr = FindTransportByRegNum(regNum);
                if (tr == null)
                {
                    CheckTariff(type);
                    ParkedTransport.Add(new Tuple<Transport, DateTime>(
                        new Transport(regNum, type, balance),
                        DateTime.Now));
                }
                else
                    throw new ApplicationException($"Transport with Registration number {regNum} is already parked!");
            }
            else
                throw new ApplicationException($"Parking is full!");

        }

        public void RemoveTransportByRegNum(string regNum)
        {
            var tr = FindTransportByRegNum(regNum);
            if (tr != null)
            {
                if (tr.ActualBalance >= 0)
                    ParkedTransport.RemoveAll(t => t.Item1 == tr);
                else
                    throw new ApplicationException($"You should repay your debt {Math.Abs(tr.ActualBalance).ToString("0.00")}$ first!");
            }
            else
            {
                throw new ApplicationException($"Transport with Registration number {regNum} is not parked!");
            }
        }

        public void DepositTransportByRegNum(string regNum, decimal bal)
        {
            var tr = FindTransportByRegNum(regNum);
            if (tr != null)
                tr.Deposit(bal);
            else
                throw new ApplicationException($"Transport with Registration number {regNum} is not parked!");
        }

        public void MakeTransportPay(Transport tr, DateTime time)
        {
            decimal tariff = CheckTariff(tr.Type);
            decimal amount = 0.00m;
            if (tr.ActualBalance - tariff >= 0)
                amount = tariff;
            else
                amount = tariff * Penalty;
            tr.Withdraw(amount);
            this.Deposit(amount);
            Transactions.Add(new Transaction(tr.RegNum, amount, time));
        }

        public List<Transaction> Transactions { get; private set; }

        public List<Transaction> TransactionsToShow
        {
            get { return Transactions.Where(t => (DateTime.Now - t.Time).TotalSeconds <= 60).ToList(); }
        }

        private Account ParkingAccount { get; set; }
        public decimal ActualBalance { get { return ParkingAccount.Balance; } }
        public void Deposit(decimal amount)
        {
            ParkingAccount.Deposit(amount);
        }

        #region Settings
        public int Capacity { get; set; }
        public int PaymentPeriod { get; set; }
        public decimal Penalty { get; set; }

        private Dictionary<string, decimal> tariffs = new Dictionary<string, decimal>();
        public Dictionary<string, decimal> Tariffs
        {
            get { return tariffs; }
            set { tariffs = value; }
        }
        #endregion
    }
}
