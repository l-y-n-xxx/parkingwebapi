﻿using parkingObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace parkingWEbAPI.Interfaces
{
    public interface IParkingService
    {
        decimal GetActualBalance();
        decimal GetLastRevenue();
        decimal GetCapacity();
        decimal GetOccupied();

        decimal CheckTariff(string type);

        void AddTransport(Transport t);
        void RemoveTransport(string num);

        void DepositTransport(string num, decimal bal);
        void ChargeTransport(Transport tr, DateTime time);

        List<Transport> GetParkedTransport();
        List<Tuple<Transport, DateTime>> GetParkedTransportAndTimes();

        List<Transaction> GetTransactions();
        List<string> ReadLogFile(string file);
        List<Transaction> GetTransactionsToBackup();
        void CleanTransactions();
    }
}
